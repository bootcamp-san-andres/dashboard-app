FROM node:12.18.0-stretch as node-server
LABEL authors="Yohon J. Bravo"
WORKDIR /bootcamp
COPY ./prod . 
COPY ./dist . 
RUN npm install --production --silent &&  mv node_modules ../ 

# Instalamos angular cli en nuestra imágen
#RUN npm install -g @angular/cli && npm cache clean
#RUN ng build --prod



EXPOSE 80
CMD ["node","server.js"]