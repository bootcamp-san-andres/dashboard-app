// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');

const PROJECT_NAME = 'bootcamp';

const bodyParser = require('body-parser');
//const proxy = require('express-http-proxy');
const cors = require('cors');
const fs = require('fs');
const app = express();


// Parsers for POST data

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

// Point static path to dist
app.use(express.static(path.join(__dirname, `./${PROJECT_NAME}`)));

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  console.log('get ...');
  res.sendFile(path.join(__dirname, `./${PROJECT_NAME}/examples/dashboard.html`));
});
/*
app.get('/test', (req, res) => {
  console.log('Test Data',req.body);
  res.json({data:"Hola Mundo"});
});*/
/**
 * Get port from environment and store in Express.
 */
 const port = '80';
app.set('port', port);
/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`Run Server https://localhost:${port}`));